<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');            
            $table->integer('id_kategori_barang')->unsigned();
            $table->foreign('id_kategori_barang')->references('id_kategori')->on('kategori')->onDelete('cascade')->onUpdate('cascade');
            $table->string('nama_barang', 100);
            $table->string('merk', 100);            
            $table->tinyInteger('qty');
            $table->enum('satuan', ['Biro','PK','Pcs', 'Packs']);
            $table->string('ruangan');
            $table->text('keterangan_barang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang');
    }
}
