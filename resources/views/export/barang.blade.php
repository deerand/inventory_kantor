<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	
<style>
.excelexport{
    border: 1px solid #000000;
}
.kepala{
    text-decoration : underline;
}
</style>
</head>
<body>
<table align="center" style="margin: 0px auto;">
	<tbody>
		
		<tr>
			<td colspan="6" align="center">KARTU INVENTARIS RUANGAN</td>
		</tr>
	</tbody>
</table>
<table>
<tbody>	
		<tr>
			<td>KABUPATEN</td>
			<td></td>
			<td>: PEMERINTAH KABUPATEN SUBANG</td>
		</tr>
		<tr>
			<td>PROVINSI</td>
			<td></td>
			<td>: JAWA BARAT</td>
		</tr>
		<tr>
			<td>UNIT</td>
			<td></td>
			<td>: DP2KBP3A</td>
		</tr>
		<tr>
			<td>SATUAN KERJA</td>
			<td></td>
			<td>: DP2KBP3A</td>
		</tr>
		<tr>
			<td>RUANGAN</td>	
			@foreach (array_slice($barang->toArray(),0,1) as $limitruangan)
			<td></td>
			<td>: {{$limitruangan->ruangan}}</td>
			@endforeach
		</tr>
	</tbody>
</table>
<table>	
	<thead>
		<tr>
			<th height="20" width="10" align="center" class="excelexport">NO URUT</th>
			<th height="20" width="35" align="center" class="excelexport">NAMA BARANG/JENIS BARANG</th>
			<th height="20" width="19" align="center" class="excelexport">MERK/MODEL</th>
			<th height="20" width="7" align="center" class="excelexport">QTY</th>
			<th height="20" width="10" align="center" class="excelexport">SATUAN</th>
			<th height="20" width="25" align="center" class="excelexport">KETERANGAN MUTASI</th>
		</tr>
	</thead>
	@foreach ($barang as $id => $barangs)
	<tbody>
		<tr>			
			<td align="center" class="excelexport">{{$id+1}}</td>
			<td align="center" class="excelexport">{{$barangs->nama_barang}}</td>
			<td align="center" class="excelexport">{{$barangs->merk}}</td>
			<td align="center" class="excelexport">{{$barangs->qty}}</td>
			<td align="center" class="excelexport">{{$barangs->satuan}}</td>
			<td align="center" class="excelexport">{{$barangs->keterangan_barang}}</td>
			
		</tr>
	</tbody>
	@endforeach
</table>
<table></table>
<table></table>
<table></table>
<table>
	<thead>
		<tr>
			<td colspan="2" align="center">MENGETAHUI</td>
		</tr>
		<tr>
			<td colspan="2" align="center">KEPALA DP2KBP3A</td>
			<td></td>
			<td></td>
			<td colspan="2" align="center">Subang,</td>
		</tr>
		<tr>
			<td colspan="2" align="center">KABUPATEN SUBANG</td>
			<td></td>
			<td></td>
			<td colspan="2" align="center">PENGURUS BARANG</td>			
		</tr>
		<tr>
			<td></td>
		</tr>
		<tr>
			<td></td>
		</tr>		
		<tr>
			<td></td>
		</tr>		
		<tr>
			<td colspan="2" align="center" class="kepala">dr.H.NUNUNG SYUHAERI , MARS</td>
			<td></td>
			<td></td>
			<td colspan="2" align="center" class="kepala">Dadang Kusna</td>					
		</tr>
		<tr>
			<td  colspan="2" align="center">NIP : 19630212 198903 1 012</td>
			<td></td>
			<td></td>
			<td colspan="2" align="center">NIP : 19621005 199103 1 006</td>					
		</tr>		
	</thead>
</table>
</body>
</html>