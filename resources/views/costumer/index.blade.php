@extends('layouts.app')
@section('content')
<br>
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-add-costumer">
  Tambah Costumer +
</button>
<br>
<br>
<table id="costumer-table" class="table table-striped">
    <thead>
        <tr>
            <th>Id</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Action</th>
        </tr>
    </thead>
</table>
<!-- Modal -->
<div class="modal fade" id="modal-add-costumer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Costumer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <div class="modal-body" data-form="simpan">
            <div class="form-group">
                <label for="nama" class="form-control-label">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama">
            </div>
            <div class="form-group">
                <label for="alamat" class="form-control-label">Alamat</label>
                <input type="text" class="form-control" id="alamat" name="alamat">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" data-btn="simpan">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
        <div class="modal fade" id="modal-update-costumer">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  <span class="sr-only">Tutup</span>
                </button>
                <h4 class="modal-title">Update Costumer</h4>
              </div>
              <div class="modal-body" data-form="update">
                <input type="hidden" name="id">
             <div class="form-group">
                <label for="nama" class="form-control-label">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama">
            </div>           
            <div class="form-group">
                <label for="alamat" class="form-control-label">Alamat</label>
                <input type="text" class="form-control" id="alamat" name="alamat">
            </div>                   
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-primary" data-btn="update">Simpan</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
</div>
        <div class="modal fade" id="modal-confirm-costumer">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  <span class="sr-only">Tutup</span>
                </button>
                <h4 class="modal-title">Konfirmasi Hapus Data</h4>
              </div>
              <div class="modal-body" data-form="delete">
                <input type="hidden" name="id">
                <p></p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-primary" data-confirm="delete">Lanjutkan</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
@endsection
@push('scripts')
  <script src="https://datatables.yajrabox.com/js/jquery.min.js"></script>    
  <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="https://datatables.yajrabox.com/js/handlebars.js"></script>  
  <script>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });
    var tablecostumer = $('#costumer-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{route('costumer.filter')}}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'nama', name: 'nama'},
            {data: 'alamat', name: 'alamat'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        initComplete: function () {
            this.api().columns([1,2]).every(function () {
                var column = this;
                $('<br><input>').appendTo($(column.header()))
                .on('keyup change blur', function () {
                    column.search($(this).val(), false, false, true).draw();
                });
            });
        }
    });    

    $('body').on('click', '[data-target="#modal-update-costumer"]', function() {
        const _id = $(this).data('id');
            $.ajax({
                url: "{{route('costumer.edit')}}",
                type: 'POST',
                dataType: 'json',
                data: {
                    _token: Laravel.csrfToken,
                    id: _id
                },
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-TOKEN', Laravel.csrfToken);
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            },
            success: function(data) {
                $('#modal-update-costumer [name="id"]').val(data.data.id);
                $('#modal-update-costumer [name="nama"]').val(data.data.nama);
                $('#modal-update-costumer [name="alamat"]').val(data.data.alamat);
            }
    });
});    
    $('body').on('click','[data-target="#modal-confirm-costumer"]',function(){
        $($(this).data('target')+' p').html('Apa Anda Yakin ingin Menghapus <strong>'+$(this).data('title')+'</strong>')
        $($(this).data('target')+' [name="id"]').val($(this).data("id"));
    });    
</script>  
@endpush