@extends('adminlte.appadminlte')
@section('isi')
	<div class="row">
		<div class="col-sm-5 col-md-6">
			<div class="box box-default">
				<div class="box-header with-border">
              		<h3 class="box-title">Barang Masuk</h3>
				</div>
			<form action="{{route('alur.store')}}" method="POST" autocomplete="off">
			{{ csrf_field() }}					
			<div class="box-body">
              <div class="form-group">
                <label>Tanggal Masuk</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right datepicker" name="tanggal">
                  <input type="hidden" name="alur" value="Masuk">
                </div>
              </div>
                <div class="form-group">
                  <label>Admin</label>
                  <input type="text" class="form-control" placeholder="{{Auth::user()->name}}" disabled>
                </div>
			<div class="baranglist1">              
                <div class="form-group">
                  <label>Keterangan Barang</label>                  
                  <input type="text" name="keterangan_barang[]" class="form-control">                  
                </div>
				<div class="form-group">
                	<label>Kuantiti</label>
                    <input type="number" class="form-control" placeholder="Enter ..." name="jumlah[]">
				</div>                 
              <button class="btn btn-primary" type="submit">Input Data</button>
              <button class="btn btn-info plus-1" type="button">+</button>
              <hr>
          </div>                
			</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="box box-default">
				<div class="box-header with-border">
              		<h3 class="box-title">Barang Keluar</h3>
				</div>
</form>        
			<form action="{{route('alur.store')}}" method="POST" autocomplete="off">
			{{ csrf_field() }}						
			<div class="box-body">
              <div class="form-group">
                <label>Tanggal Keluar</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right datepicker" name="tanggal">
                  <input type="hidden" name="alur" value="Keluar">
                </div>
              </div>
                <div class="form-group">
                  <label>Admin</label>
                  <input type="text" class="form-control" placeholder="{{Auth::user()->name}}" disabled>
                </div>              
			<div class="baranglist2">              
                <div class="form-group">
                  <label>Keterangan Barang</label>                  
                  <input type="text" name="keterangan_barang[]" class="form-control">                  
                </div>
				<div class="form-group">
                	<label>Kuantiti</label>
                    <input type="number" class="form-control" placeholder="Enter ..." name="jumlah[]">
				</div>                 
              <button class="btn btn-primary" type="submit">Input Data</button>
              <button class="btn btn-info plus-2" type="button">+</button>
              <hr>
          </div>
          </form>
			</div>
			</div>
		</div>
		</div>		
	</div>
@endsection
@push('scripts')
<script src={{asset('js/jquery.js')}}></script>
<script src={{asset('js/typeahead.min.js')}}></script>
<script src={{asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}></script>
<!--<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>-->

<script type="text/javascript">
$(document).ready(function () {
  $('.datepicker').datepicker({
    autoclose: true,
    format: "yyyy-mm-dd",
    todayHighlight: true,
    orientation: "bottom auto",
    todayBtn: true,
  });
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  function reinit_type() {
    $('[name="keterangan_barang[]"]').typeahead({
      source: function (request, response) {
        $.ajax({
          url: "{{ route('typeahead.barang') }}",
          dataType: 'JSON',
          data: {
            state: Laravel.csrfToken
          },
          success: function (data) {
            response(data);
          }
        });
      },
      hint: true,
      highlight: true,
      minLength: 1,
      highlighter: Object,
      autoSelect: true,
      displayText: function (item) {
        return item.value + ' | ' + item.ket;
      },
      updater: function (item) {
        localStorage.setItem('barang_id', item.id);
        return item;
      }
    });
  }
  $('body').on('click', '.plus-1', function () {

    $('.baranglist1').append('<div class="box-body">' +
      '<div class="form-group">' +
      '<label>Keterangan Barang</label>' +
      '<input type="hidden" name="type" value="income">' +
      '<input type="text" name="keterangan_barang[]" class="form-control">' +
      '</div>' +
      '<div class="form-group">' +
      '<label>Kuantiti</label>' +
      '<input type="number" class="form-control" placeholder="Enter ..." name="jumlah[]">' +
      '</div>' +
      '<button class="btn btn-primary" type="submit">Input Data</button>' +
      '<button class="btn btn-info plus-1" type="button">+</button>' +
      '<hr>' +
      '</div>');
    $(this).text('-').addClass('min-1 btn-danger').removeClass('plus-1 btn-info');
    reinit_type();
  });
  $('body').on('click', '.plus-2', function () {

    $('.baranglist2').append('<div class="box-body">' +
      '<div class="form-group">' +
      '<label>Keterangan Barang</label>' +
      '<input type="hidden" name="type" value="income">' +
      '<input type="text" name="keterangan_barang[]" class="form-control">' +
      '</div>' +
      '<div class="form-group">' +
      '<label>Kuantiti</label>' +
      '<input type="number" class="form-control" placeholder="Enter ..." name="jumlah[]">' +
      '</div>' +
      '<button class="btn btn-primary" type="submit">Input Data</button>' +
      '<button class="btn btn-info plus-2" type="button">+</button>' +
      '<hr>' +
      '</div>');
    $(this).text('-').addClass('min-2 btn-danger').removeClass('plus-2 btn-info');
    reinit_type();
  });
  $('body').on('change', '[name="keterangan_barang[]"]', function () { // geus teu kudu
    const _self = $(this);
    const current = _self.typeahead("getActive");
    if (current) {
      // Some item from your model is active!
      if (current.id === parseInt(localStorage.getItem('barang_id'))) {
        _self.parent().find('[name="id_barang[]"]').remove();
        _self.parent().append('<input type="hidden" name="id_barang[]" value="' + current.id + '">');
        localStorage.removeItem('barang_id');
      }
    }
  });
  $('body').on('click', '.min-1', function () {
    $(this).parent().remove()
  });
  $('body').on('click', '.min-2', function () {
    $(this).parent().remove()
  });
  reinit_type();
});
</script>
@endpush