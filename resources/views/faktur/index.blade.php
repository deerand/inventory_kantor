@extends('layouts.app')
@section('content')
<br>
<div class="row">
	<div class="col-sm-6">
		<div class="card">
			<div class="card-body">
        <form action="{{route('faktur.store')}}" method="POST" autocomplete="off">
        {{ csrf_field() }}
        <input type="hidden" name="type" value="income">
        <h4 class="card-title">Form Barang Masuk</h4>
        <label for="tanggalfakturmasuk">Tanggal Transaksi</label>
				<input type="text" class="form-control" id="datepickermasuk" placeholder="Pilih Tanggal" name="tgl_trx">
				<small class="text-muted">Format Tanggal : DD/MM/YY</small>
			<div class="prodlist-1">
     <div class="row">
        <div class="col-sm">
          <div class="form-group">
            <input type="text" class="form-control" name="produk[]" value="" placeholder="Produk1">
          </div>
        </div>
          <div class="col-sm"><!--cobaan-->
          <div class="form-group"><input type="text" class="form-control" name="qty[]" value="" placeholder="Qty">
          </div>
        </div>
          <div class="col-sm">
            <button class="btn btn-info plus-1" type="button">+</button>
          </div>
      </div>   
      </div>
        		<button class="btn btn-primary" type="submit">Buat Faktur Barang Masuk</button>
        		<button class="btn btn-basic">Batal</button>
      		</div>
          </form>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <form action="{{route('faktur.store')}}" method="POST" autocomplete="off">
        {{ csrf_field() }}
        <input type="hidden" name="type" value="outcome">
        <h4 class="card-title">Form Barang Keluar</h4>
        <label for="tanggalfakturkeluar">Tanggal Transaksi</label>
    		<input type="text" class="form-control" id="datepickerkeluar" placeholder="Pilih Tanggal">
    		<small class="text-muted">Format Tanggal : DD/MM/YY</small>
      <div class="prodlist-2">
     <div class="row">
        <div class="col-sm">
          <div class="form-group">
            <input type="text" class="form-control" name="produk[]" value="" placeholder="Produk1">
          </div>
        </div>
          <div class="col-sm">
          <div class="form-group"><input type="text" class="form-control" name="qty[]" value="" placeholder="Qty">
          </div>
        </div>
          <div class="col-sm">
            <button class="btn btn-info plus-2" type="button">+</button>
          </div>
      </div>   
      </div>   
        <button class="btn btn-primary" type="submit">Buat Faktur Barang Keluar</button>
        <button class="btn btn-basic">Batal</button>   
        </form>     
      </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function () {
        $('#datepickermasuk').datepicker({
            format: "dd/mm/yyyy"
        });        
        $('#datepickerkeluar').datepicker({
            format: "dd/mm/yyyy"
        });
        function reinit_type() {
          $('[name="produk[]"]').typeahead('destroy');
          $('[name="produk[]"]').typeahead({
           source: function(request, response) {
               $.ajax({
                   url: "{{ route('produk.all') }}",
                   dataType: "json",
                   data: {
                        state: Laravel.csrfToken
                    },
                   success: function (data) {
                        response(data);
                   }
             });
          },
         minLength: 1,
         updater: function (item) {
          localStorage.setItem('prod_id', item.id);
          return item;
         }
     });
        }
     $('body').on('click', '.plus-1', function() {
       $('.prodlist-1').append('<div class="row">\
        <div class="col-sm">\
          <div class="form-group">\
            <input type="text" class="form-control" name="produk[]" value="" placeholder="Produk1">\
          </div>\
        </div>\
          <div class="col-sm">\
          <div class="form-group"><input type="text" class="form-control" name="qty[]" value="" placeholder="Qty">\
          </div>\
        </div>\
          <div class="col-sm">\
            <button class="btn btn-info plus-1" type="button">+</button>\
          </div>\
      </div>');
       $(this).text('-').addClass('min-1 btn-danger').removeClass('plus-1 btn-info');
       reinit_type();
     });
     $('body').on('click' , '.min-1',function()
      {
        $(this).parent().parent().remove()
      });
     $('body').on('click' , '.min-2',function()
      {
        $(this).parent().parent().remove()
      });  
      $('body').on('change', '[name="produk[]"]', function() { // geus teu kudu
        const _self = $(this);
        const current = _self.typeahead("getActive");
        if (current) {
          // Some item from your model is active!
          if (current.id === parseInt(localStorage.getItem('prod_id'))) {
            _self.parent().find('[name="id[]"]').remove();
            _self.parent().append('<input type="hidden" name="id[]" value="'+current.id+'">');
            localStorage.removeItem('prod_id');
          }
        }
      });   
     $('body').on('click', '.plus-2', function() {
       $('.prodlist-2').append('<div class="row">\
        <div class="col-sm">\
          <div class="form-group">\
            <input type="text" class="form-control" name="produk[]" value="" placeholder="Produk1">\
          </div>\
        </div>\
          <div class="col-sm">\
          <div class="form-group"><input type="text" class="form-control" name="qty[]" value="" placeholder="Qty">\
          </div>\
        </div>\
          <div class="col-sm">\
            <button class="btn btn-info plus-2" type="button">+</button>\
          </div>\
      </div>');
       $(this).text('-').addClass('min-2 btn-danger').removeClass('plus-2 btn-info');
       reinit_type();
     });
     reinit_type();
    });
</script>
@endpush