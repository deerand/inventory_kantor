@extends('adminlte.appadminlte')

@section('isi')
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Pengolahan</h3>
            </div>
            <div class="box-body">
      <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-tambah">
          Tambah Barang
      </button>
      <div class="form-group">
        <label>Export Excel Berdasarkan Ruangan</label>
          <select class="form-control" name="ruanganexcel" id="ruanganexcel">
            <option value="Kepala Dinas">Kepala Dinas</option>
            <option value="Bidang Dalduk">Bidang Dalduk</option>
            <option value="Staf Dalduk">Staf Dalduk</option>
            <option value="Bidang Pembangunan">Bidang Pembangunan</option>
            <option value="Ruangan Perlindungan Anak">Ruangan Perlindungan Anak</option>
            <option value="Bidang Perlindungan Anak">Bidang Perlindungan Anak</option>
            <option value="Bidang KB">Bidang KB</option>
            <option value="Staf Sekretaris">Staf Sekretaris</option>
            <option value="Keuangan">Keuangan</option>
            <option value="Sekretaris">Sekretaris</option>
            <option value="Dharma Wanita">Dharma Wanita</option>
            <option value="Salasar">Salasar</option>
            <option value="Bidang Pemberdayaan Perempuan">Bidang Pemberdayaan Perempuan</option>
            <option value="Staf Perlindungan Perempuan">Staf Perlindungan Perempuan</option>
            <option value="Staf Bidang KB">Staf Bidang KB</option>
          </select>
      </div>
      <button type="button" class="btn btn-info" id="exportexcelruangan">
      Download Excel Berdasarkan Ruangan      
      </button>      
      </br></br>     
            <table id="barang-table" class="table table-hover">
    <thead>
        <tr>            
            <th>Nama Barang</th>
            <th>Merk</th>
            <th>Qty</th>
            <th>Satuan</th>
            <th>Ruangan</th>
            <th>Tanggal Masuk</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot>
        <tr>            
            <th>Nama Barang</th>
            <th>Merk</th>
            <th>Qty</th>
            <th>Satuan</th>
            <th>Ruangan</th>
            <th>Tanggal Masuk</th>
            <th>Aksi</th>
        </tr>
    </tfoot>
    </table>   
            </div>
          </div>
        </div>
      </div>

        <div class="modal fade" id="modal-tambah">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Barang</h4>
              </div>
              <div class="modal-body">                                  
                  <div class="form-group">
                    <label>Admin</label>
                    <select class="form-control" disabled name="id_user">
                      <option value="{{ Auth::user()->id }}">{{ Auth::user()->name }}</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Kategori</label>
                    <select class="form-control" name="id_kategori_barang">
                      @foreach ($data as $datas)
                        <option value="{{$datas->id_kategori}}">{{$datas->nama_kategori}}</option>
                      @endforeach
                      
                    </select>
                    </div>
                    <div class="form-group">
                      <label>Nama Barang</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="nama_barang">
                    </div>
                    <div class="form-group">
                      <label>Merek</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="merk">
                    </div>
                    <div class="form-group">
                      <label>Kuantiti</label>
                      <input type="number" class="form-control" placeholder="Enter ..." name="qty">
                    </div>                    
                <div class="form-group">
                  <label>Satuan</label>
                  <select class="form-control" name="satuan">
                    <option value="Biro">Biro</option>
                    <option value="Packs">Packs</option>
                    <option value="Pcs">Pcs</option>
                    <option value="PK">PK</option>                  
                  </select>
                </div>
                <div class="form-group">
                  <label>Ruangan</label>
                  <select class="form-control" name="ruangan">
                    <option value="Kepala Dinas">Kepala Dinas</option>
                    <option value="Bidang Dalduk">Bidang Dalduk</option>
                    <option value="Staf Dalduk">Staf Dalduk</option>
                    <option value="Bidang Pembangunan">Bidang Pembangunan</option>
                    <option value="Ruangan Perlindungan Anak">Ruangan Perlindungan Anak</option>
                    <option value="Bidang Perlindungan Anak">Bidang Perlindungan Anak</option>
                    <option value="Bidang KB">Bidang KB</option>
                    <option value="Staf Sekretaris">Staf Sekretaris</option>
                    <option value="Keuangan">Keuangan</option>
                    <option value="Sekretaris">Sekretaris</option>
                    <option value="Dharma Wanita">Dharma Wanita</option>
                    <option value="Salasar">Salasar</option>
                    <option value="Bidang Pemberdayaan Perempuan">Bidang Pemberdayaan Perempuan</option>
                    <option value="Staf Perlindungan Perempuan">Staf Perlindungan Perempuan</option>
                    <option value="Staf Bidang KB">Staf Bidang KB</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Keterangan Barang</label>
                  <textarea class="form-control" rows="3" placeholder="Enter ..." name="keterangan_barang"></textarea>
                </div>                
              </div>                
              
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="simpan_barang">Save changes</button>
              </div>            
              </div>            
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="modal-update">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Barang</h4>
              </div>
              <div class="modal-body">
                  <div class="form-group">
                    <label>Admin</label>
                    <select class="form-control" disabled name="id_user">
                      <option value="{{ Auth::user()->id }}">{{ Auth::user()->name }}</option>
                      <input type="hidden" class="form-control" placeholder="Enter ..." name="id">
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Kategori</label>
                    <select class="form-control" name="id_kategori_barang">
                      @foreach ($data as $datas)
                        <option value="{{$datas->id_kategori}}">{{$datas->nama_kategori}}</option>
                      @endforeach
                      
                    </select>
                    </div>
                    <div class="form-group">
                      <label>Nama Barang</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="nama_barang">
                    </div>
                    <div class="form-group">
                      <label>Merek</label>
                      <input type="text" class="form-control" placeholder="Enter ..." name="merk">
                    </div>
                    <div class="form-group">
                      <label>Kuantiti</label>
                      <input type="number" class="form-control" placeholder="Enter ..." name="qty">
                    </div>                    
                <div class="form-group">
                  <label>Satuan</label>
                  <select class="form-control" name="satuan">
                    <option value="Biro">Biro</option>
                    <option value="Packs">Packs</option>
                    <option value="Pcs">Pcs</option>
                    <option value="PK">PK</option>                  
                  </select>
                </div>
                <div class="form-group">
                  <label>Ruangan</label>
                  <select class="form-control" name="ruangan">
                    <option value="Kepala Dinas">Kepala Dinas</option>
                    <option value="Bidang Dalduk">Bidang Dalduk</option>
                    <option value="Staf Dalduk">Staf Dalduk</option>
                    <option value="Bidang Pembangunan">Bidang Pembangunan</option>
                    <option value="Ruangan Perlindungan Anak">Ruangan Perlindungan Anak</option>
                    <option value="Bidang Perlindungan Anak">Bidang Perlindungan Anak</option>
                    <option value="Bidang KB">Bidang KB</option>
                    <option value="Staf Sekretaris">Staf Sekretaris</option>
                    <option value="Keuangan">Keuangan</option>
                    <option value="Sekretaris">Sekretaris</option>
                    <option value="Dharma Wanita">Dharma Wanita</option>
                    <option value="Salasar">Salasar</option>
                    <option value="Bidang Pemberdayaan Perempuan">Bidang Pemberdayaan Perempuan</option>
                    <option value="Staf Perlindungan Perempuan">Staf Perlindungan Perempuan</option>
                    <option value="Staf Bidang KB">Staf Bidang KB</option>                 
                  </select>
                </div>
                <div class="form-group">
                  <label>Keterangan Barang</label>
                  <textarea class="form-control" rows="3" placeholder="Enter ..." name="keterangan_barang"></textarea>
                </div>                
              </div>                
              
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="update_barang">Save changes</button>
              </div>            
              </div>            
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>        
        <!-- /.modal -->
        <div class="modal fade" id="modal-confirm-delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modal Confirm Delete</h4>
              </div>
              <div class="modal-body" data-form="delete">
                <p></p>
                <input type="hidden" name="id">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" id="hapus_data">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->        
@endsection
<script src={{asset('js/jquery.min.js')}}></script>
<script type="text/javascript">
$(document).ready(function () {
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

  $('#simpan_barang').click(function(e){
        e.preventDefault(); 
        var formData = {
            id_user: $('[name="id_user"]').val(),
            id_kategori_barang: $('[name="id_kategori_barang"]').val(),
            nama_barang: $('[name="nama_barang"]').val(),
            merk: $('[name="merk"]').val(),
            qty: $('[name="qty"]').val(),
            satuan: $('[name="satuan"]').val(),
            ruangan: $('[name="ruangan"]').val(),
            keterangan_barang: $('[name="keterangan_barang"]').val(),
        }
      console.log(formData);        
        $.ajax({
            type: "POST",
            url: "{{route('barang.store')}}",
            data: formData,
            dataType: 'json',
            beforeSend: function(xhr) {
              xhr.setRequestHeader('X-CSRF-TOKEN', Laravel.csrfToken);
              xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            },            
            success: function (data) {
                console.log(data);
                toastr.success("Simpan Data Berhasil","Notifikasi",{timeOut: 5000});
                $('#modal-tambah').modal('hide');
                $('#barang-table').DataTable().ajax.reload(null, false).draw();
            },
        error: function(jqXhr, json, errorThrown)
        {
          var errors= jqXhr.responseJSON;
              var errorsHTML='';
              $.each(errors, function(key, value){
                  errorsHTML +='<li>' + value[0] +'</li>'
              });
              toastr.error(errorsHTML, "Error"+ jqXhr.status +': '+ errorThrown);
        }


  });
      });
    $('#barang-table').DataTable({
        language : {
          sProcessing:   "Sedang memproses...",
          sLengthMenu:   "Tampilkan _MENU_ Entri",
          sZeroRecords:  "Tidak ditemukan data yang sesuai",
          sInfo:         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
          sInfoEmpty:    "Menampilkan 0 sampai 0 dari 0 entri",
          sInfoFiltered: "(disaring dari _MAX_ entri keseluruhan)",
          sInfoPostFix:  "",
          sSearch:       "Cari:",
          sUrl:          "",
          oPaginate: {
          sFirst:    "Pertama",
          sPrevious: "Sebelumnya",
          sNext:     "Selanjutnya",
          sLast:     "Terakhir"
          }
        },            
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{route('barang.datatable')}}',
            method: 'POST',
            dataType: 'json',
        },
        beforeSend: function(xhr) {
          xhr.setRequestHeader('X-CSRF-TOKEN', Laravel.csrfToken);
          xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        },        
        columns: [            
            {data: 'nama_barang', name: 'nama_barang'},
            {data: 'merk', name: 'merk'},
            {data: 'qty', name: 'qty'},
            {data: 'satuan', name: 'satuan'},
            {data: 'ruangan', name: 'ruangan'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        autoWidth : true,
        initComplete: function () {
            this.api().columns([0,1,3,5]).every(function () {
                var column = this;
                var input = document.createElement("input");
                $(input).appendTo($(column.footer()).empty())
                .on('change', function () {
                    column.search($(this).val()).draw();
                });
            });
        }
    });  
    $('body').on('click','[data-target="#modal-confirm-delete"]',function(){
        $($(this).data('target')+' p').html('Apa Anda Yakin ingin Menghapus <strong>'+$(this).data('title')+'</strong>')
        $($(this).data('target')+' [name="id"]').val($(this).data("id"));
    });
    $('body').on('click','#hapus_data',function(){
    const _form = $('[data-form="delete"]');
    $.ajax({
      url: "{{route('barang.hapus')}}",
      type: 'POST',
      dataType: 'json',
      data: {
        _token: Laravel.csrfToken,
        id: _form.find('[name="id"]').val()
      },
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-CSRF-TOKEN', Laravel.csrfToken);
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      },
      success: function(data) {
        $('#modal-confirm-delete').modal('hide');
        $('#barang-table').DataTable().ajax.reload(null, false).draw();
        toastr.success("Data Berhasil di Hapus", "Notifikasi");
      },
      error: function(jqXhr, json, errorThrown)
      {
        var errors= jqXhr.responseJSON;
            var errorsHTML='';
            $.each(errors, function(key, value){
                errorsHTML +='<li>' + value[0] +'</li>'
            });
            toastr.error(errorsHTML, "Error"+ jqXhr.status +': '+ errorThrown);
      }
    });
    });
  $('body').on('click', '[data-target="#modal-update"]', function() {
    const _id = $(this).data('id');
    $.ajax({
      url: "{{route('barang.edit')}}",
      type: 'POST',
      dataType: 'json',
      data: {
        _token: Laravel.csrfToken,
        id: _id
      },
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-CSRF-TOKEN', Laravel.csrfToken);
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      },
      success: function(data) {
        $('#modal-update [name="id"]').val(data.id);
        $('#modal-update [name="id_user"]').val(data.id_user);
        $('#modal-update [name="id_kategori_barang"]').val(data.id_kategori_barang);
        $('#modal-update [name="nama_barang"]').val(data.nama_barang);
        $('#modal-update [name="merk"]').val(data.merk);
        $('#modal-update [name="qty"]').val(data.qty);
        $('#modal-update [name="satuan"]').val(data.satuan);
        $('#modal-update [name="ruangan"]').val(data.ruangan);
        $('#modal-update [name="keterangan_barang"]').val(data.keterangan_barang);      
      }
    });
  });
  $('#update_barang').click(function(e){
        e.preventDefault(); 
        var formData = {
            id: $('#modal-update [name="id"]').val(),
            id_user: $('#modal-update [name="id_user"]').val(),
            id_kategori_barang: $('#modal-update [name="id_kategori_barang"]').val(),
            nama_barang: $('#modal-update [name="nama_barang"]').val(),
            merk: $('#modal-update [name="merk"]').val(),
            qty: $('#modal-update [name="qty"]').val(),
            satuan: $('#modal-update [name="satuan"]').val(),
            ruangan: $('#modal-update [name="ruangan"]').val(),
            keterangan_barang: $('#modal-update [name="keterangan_barang"]').val(),
        }
      console.log(formData);        
        $.ajax({
            type: "POST",
            url: "{{route('barang.update')}}",
            data: formData,
            dataType: 'json',
            beforeSend: function(xhr) {
              xhr.setRequestHeader('X-CSRF-TOKEN', Laravel.csrfToken);
              xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            },            
            success: function (data) {
                console.log(data);
                toastr.success("Update Data Berhasil","Notifikasi",{timeOut: 5000});
                $('#modal-update').modal('hide');
                $('#barang-table').DataTable().ajax.reload(null, false).draw();
            },
        error: function(jqXhr, json, errorThrown)
        {
          var errors= jqXhr.responseJSON;
              var errorsHTML='';
              $.each(errors, function(key, value){
                  errorsHTML +='<li>' + value[0] +'</li>'
              });
              toastr.error(errorsHTML, "Error"+ jqXhr.status +': '+ errorThrown);
        }
  });    
  });
    $('body').on('click','#exportexcelruangan',function(){
    var ruangan = $('#ruanganexcel').val();
    $.ajax({
          cache: false,
          url: '{{url('export/barang')}}'+'/'+ruangan, //GET route 
          data:  ruangan, //your parameters data here
          success: function (response, textStatus, request) {
            var a = document.createElement("a");
            a.href = response.file; 
            a.download = response.name;
            document.body.appendChild(a);
            a.click();
            a.remove();
            toastr.success('Download Excel Berhasil','Notifikasi',{timeOut: 5000});
          },
          error: function (ajaxContext) {
            toastr.error('Download Excel Gagal','Notifikasi',{timeOut: 5000});
          }
        });
    });
});
</script>