<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriModel extends Model
{
	protected $table = 'kategori';
	protected $primaryKey = 'id_kategori';
    protected $fillable = [
        'id_kategori', 'id_user','nama_kategori',
    ];
    public function barang()
    {
        return $this->hasMany('App\BarangModel');
    }        
}
