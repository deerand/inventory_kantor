<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarangModel extends Model
{
	protected $table = 'barang';	
    protected $fillable = [
        'id_user', 'id_kategori_barang', 'nama_barang','merk','qty','ukuran','ruangan','keterangan_barang'
    ];
    public function ketegori()
    {
        return $this->belongsTo('App\KategoriModel');
    }
}
