<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlurBarangModel extends Model
{
	protected $table = 'alurbarang';	
    protected $fillable = [
        'no_faktur_alur','id_user', 'id_barang','jumlah'
    ];   
}
