<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FakturModel extends Model
{
	protected $table = 'fakturbarang';
	protected $primaryKey = 'no_faktur';
    protected $fillable = [
        'no_faktur','id_user', 'tanggal','alur'
    ];       
}
