<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use Auth;
use DB;
use App\ProdukModel;
class Produk extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('produk.index');
    }
    public function lizt()
        {
            $produks = ProdukModel::all();
            foreach ($produks as &$row)
            {
                $row['name'] = $row['nama'];
                unset( $row['nama'] );
            }
           return response()->json($produks);
        }    
    public function getMultiFilterSelectData()
    {
        $users = ProdukModel::select([
            'id',
            'uniq_kode',
            'nama',
            'qty',
            'satuan'
        ]);

        return Datatables::of($users)
            ->filterColumn('produk_id', function($query, $keyword) {
                $query->whereRaw("CONCAT(produk.id,'-',produk.id) like ?", ["%{$keyword}%"]);
            })->addColumn('action', function ($user) {
                return '<button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-update-produk" data-id="'.$user->id.'">Ubah</button>
          <button class="btn btn-danger btn-sm" data-toggle="modal" data-title="'.$user->nama.'"data-target="#modal-confirm-produk" data-id="'.$user->id.'">Delete</button>';
        })->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'uniq_kode' => 'required',
            'nama' => 'required',
            'qty' => 'required',
            'satuan' => 'required',
        ]);     
        $data = New ProdukModel;
        $data->id_users_produk=Auth::user()->id;
        $data->uniq_kode = $request->input('uniq_kode');
        $data->nama = $request->input('nama');
        $data->qty = $request->input('qty');
        $data->satuan = $request->input('satuan');
        $data->save();
        $status=200;
        return response()->json($data,$status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $this->validate($request,[      
            'id' => 'Required',
            ]);     
        $this->content['data'] = ProdukModel::find($request->input('id'));
        $status=200;

        return Response()->json($this->content,$status);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->validate($request,[
            'id' => 'Required',
            ]);

        $this->content['data'] = ProdukModel::find($request->input('id'))->delete();
        $status=200;

        return Response()->json($this->content,$status);
    }
}
