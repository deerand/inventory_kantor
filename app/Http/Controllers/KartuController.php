<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\BarangModel;
use DB;
class KartuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function exportbarang($ruangan)
    {
        $barang = DB::table('barang')->where('ruangan',$ruangan)->get();
		$myFile = Excel::create('New file', function($excel) use ($barang){
			$excel->sheet('New sheet', function($sheet) use ($barang) {
                $sheet->setOrientation('landscape');
        		$sheet->loadView('export.barang')->with('barang',$barang);

    		});
		});
        $myFile = $myFile->string('xlsx'); //change xlsx for the format you want, default is xls
        $response =  array(
           'name' => "Kartu Inventaris Ruangan DP2KBP3A ".now(), //no extention needed
           'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,".base64_encode($myFile) //mime type of used format
        );
        return response()->json($response);        
    }
}
