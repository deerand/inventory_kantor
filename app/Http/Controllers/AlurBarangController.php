<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\AlurBarangModel;
use App\BarangModel;
use App\FakturModel;
use DateTime;
use Carbon\Carbon;
class AlurBarangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	return view ('alurbarang.index');
    }
    public function autocomplete(Request $request)
    {        
        $query = $request->get('query','');

        $posts = BarangModel::select('id','nama_barang','ruangan')->where('nama_barang','LIKE','%'.$query.'%')->get();
        foreach($posts as $row)
        {	
        	$data[]=array(
        		'value'=>$row->nama_barang,
        		'ket' =>$row->ruangan,
        		'id'=>$row->id);
        }       
        return response()->json($data);        
    }
    function store(Request $request)
    {
		$invoice = new FakturModel();
		$lastInvoiceID = $invoice->orderBy('id', 'DESC')->pluck('id')->first();
		$newInvoiceID = $lastInvoiceID + 1;        	
		$faktur = FakturModel::create([
			'no_faktur' => 'F-'.$newInvoiceID,
			'id_user' => Auth::user()->id,
			'tanggal' => $request->input('tanggal'),
			'alur' => $request->input('alur')
		]);    	
		$barang = $request->input('id_barang');
		$qty = $request->input('jumlah');

		foreach ($barang as $k => $item) {
			$barang = BarangModel::find($item);
			if($request->input('alur') != 'Masuk'){
				$barang->qty = $barang->qty - $qty[$k];			
			}else{
				$barang->qty = $barang->qty + $qty[$k];
			}
			$barang->save();			
			AlurBarangModel::create([
				'no_faktur_alur' => 'F-'.$newInvoiceID,
				'id_user' => Auth::user()->id,
				'id_barang' => $barang->id,
				'jumlah' => $qty[$k],
			]);
		}
		return redirect('alurbarang');
    }
}
