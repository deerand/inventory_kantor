<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BarangModel;
use App\KategoriModel;
use Auth;
use Datatables;

class BarangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$data = KategoriModel::get();
    	return view ('barang.index',['data' => $data]);
    }
    public function anyColumnSearchData(Request $request)
    {
        $users = BarangModel::select([
        	'id',
            'nama_barang',
            'merk',
            'qty',
            'satuan',
            'ruangan',
            'created_at'
        ]);

        return Datatables::of($users)
            ->addColumn('action', function ($user) {
			return '<button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-update" data-id="'.$user->id.'">Ubah</button>
          <button class="btn btn-danger btn-sm" data-toggle="modal" data-title="'.$user->nama_barang.'"data-target="#modal-confirm-delete" data-id="'.$user->id.'">Hapus</button>';
        })->make(true);
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'id_user' => 'required',
            'id_kategori_barang' => 'required',
            'nama_barang' => 'required',
            'merk' => 'required',
            'qty' => 'required',
            'satuan' => 'required',
            'ruangan' => 'required',
            'keterangan_barang' => 'required',
        ]);     
        $data = New BarangModel;
        $data->id_user=Auth::user()->id;
        $data->id_kategori_barang = $request->input('id_kategori_barang');
        $data->nama_barang = $request->input('nama_barang');
        $data->merk = $request->input('merk');
        $data->qty = $request->input('qty');
        $data->satuan = $request->input('satuan');
        $data->ruangan = $request->input('ruangan');
        $data->keterangan_barang = $request->input('keterangan_barang');
        $data->save();
        $status=200;
        return response()->json($data,$status);
    }
    public function destroy(Request $request)
    {
        $this->validate($request,[
            'id' => 'Required',
            ]);

        $this->content['data'] = BarangModel::find($request->input('id'))->delete();
        $status=200;
        
        return Response()->json($this->content,$status);
    }
    public function find(Request $request)
    {
    	$this->validate($request,[    	
    		'id' => 'Required',
    		]);   	
    	$data = BarangModel::find($request->input('id'));
    	$status=200;

    	return Response()->json($data,$status);
    }
    public function update(Request $request)
    {
    	$this->validate($request,[
            'id_user' => 'required',
            'id_kategori_barang' => 'required',
            'nama_barang' => 'required',
            'merk' => 'required',
            'qty' => 'required',
            'satuan' => 'required',
            'ruangan' => 'required',
            'keterangan_barang' => 'required',
    		]);

    	$this->content['data'] = BarangModel::find($request->input('id'))
    	->update([
    		'id_user' => Auth::user()->id,
    		'id_kategori_barang' => $request->input('id_kategori_barang'),
    		'nama_barang' => $request->input('nama_barang'),
    		'merk' => $request->input('merk'),
    		'qty' => $request->input('qty'),
    		'satuan' => $request->input('satuan'),
    		'ruangan' => $request->input('ruangan'),
    		'keterangan_barang' => $request->input('keterangan_barang')
    		]);
    	$status=200;

    	return Response()->json($this->content,$status);
    }    

}
