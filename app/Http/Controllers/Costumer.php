<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use App\CostumerModel;
class Costumer extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('costumer.index');
    }    
    public function getMultiFilterSelectData()
    {
        $this->content['data'] = CostumerModel::select(['id', 'nama', 'alamat']);

        return Datatables::of($this->content['data'])
            ->addColumn('action', function ($costumer) {
                return '
                    <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-update-costumer" data-id="'.$costumer->id.'">Ubah</button>
                    <button class="btn btn-danger btn-sm" data-toggle="modal" data-title="'.$costumer->nama.'"data-target="#modal-confirm-costumer" data-id="'.$costumer->id.'">Delete</button>';
            })->make(true);
        $status=200;            
		return Response()->json($this->content,$status);            
            
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $this->validate($request,[      
            'id' => 'Required',
            ]);     
        $this->content['data'] = CostumerModel::find($request->input('id'));
        $status=200;

        return Response()->json($this->content,$status);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
