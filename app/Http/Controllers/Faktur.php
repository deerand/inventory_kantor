<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FakturModel;
use App\DetailModel;
use App\ProdukModel;
use Auth;
use Carbon\Carbon;
class Faktur extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	return view('faktur.index');
    }
    public function store(Request $request)
    {
		$lastInvoiceID = (FakturModel::orderBy('id', 'DESC')->pluck('id')->first()) ? FakturModel::orderBy('id', 'DESC')->pluck('id')->first() : 0;
		$newInvoiceID = $lastInvoiceID + 1;
		$invoice = FakturModel::create([
			'no_faktur' => 'F-'.$newInvoiceID,
			'id_costumer' => 1,
			'id_users' => Auth::user()->id,
			'tgl_transaksi' => ($request->input('tgl_trx')) ? Carbon::createFromFormat('d/m/Y', $request->input('tgl_trx')) : Carbon::now(),
			'type_transaksi' => $request->input('type')
		]);
		$produk = $request->input('id');
		$qty = $request->input('qty');

		foreach ($produk as $k => $item) {
			$produk = ProdukModel::find($item);
			if($request->input('type') != 'income'){
				$produk->qty = $produk->qty - $qty[$k];
			}else{
				$produk->qty = $produk->qty + $qty[$k];
			}
			$produk->save();

			DetailModel::create([
				'id_produk' => $produk->id,
				'no_faktur_detail' => $invoice->no_faktur,
				'qty' => $qty[$k],
			]);
		}
		return redirect('faktur');
    }
    
}
