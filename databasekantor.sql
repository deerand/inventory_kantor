/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.1.30-MariaDB : Database - inventory_kantor
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`inventory_kantor` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `inventory_kantor`;

/*Table structure for table `alurbarang` */

DROP TABLE IF EXISTS `alurbarang`;

CREATE TABLE `alurbarang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `no_faktur_alur` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `id_barang` int(10) unsigned NOT NULL,
  `jumlah` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `alurbarang_no_faktur_alur_foreign` (`no_faktur_alur`),
  KEY `alurbarang_id_user_foreign` (`id_user`),
  KEY `alurbarang_id_barang_foreign` (`id_barang`),
  CONSTRAINT `alurbarang_id_barang_foreign` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `alurbarang_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `alurbarang_no_faktur_alur_foreign` FOREIGN KEY (`no_faktur_alur`) REFERENCES `fakturbarang` (`no_faktur`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `alurbarang` */

/*Table structure for table `barang` */

DROP TABLE IF EXISTS `barang`;

CREATE TABLE `barang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(10) unsigned NOT NULL,
  `id_kategori_barang` int(10) unsigned NOT NULL,
  `nama_barang` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `merk` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` tinyint(4) NOT NULL,
  `satuan` enum('Biro','PK','Pcs','Packs') COLLATE utf8mb4_unicode_ci NOT NULL,
  `ruangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan_barang` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `barang_id_user_foreign` (`id_user`),
  KEY `barang_id_kategori_barang_foreign` (`id_kategori_barang`),
  CONSTRAINT `barang_id_kategori_barang_foreign` FOREIGN KEY (`id_kategori_barang`) REFERENCES `kategori` (`id_kategori`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `barang_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `barang` */

insert  into `barang`(`id`,`id_user`,`id_kategori_barang`,`nama_barang`,`merk`,`qty`,`satuan`,`ruangan`,`keterangan_barang`,`created_at`,`updated_at`) values 
(6,2,1,'Pensil','Staedtler',1,'Pcs','Kepala Dinas','Kepala Dinas','2019-01-29 07:36:33','2019-02-03 04:50:07');

/*Table structure for table `fakturbarang` */

DROP TABLE IF EXISTS `fakturbarang`;

CREATE TABLE `fakturbarang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `no_faktur` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `alur` enum('Masuk','Keluar') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fakturbarang_no_faktur_unique` (`no_faktur`),
  KEY `fakturbarang_id_user_foreign` (`id_user`),
  CONSTRAINT `fakturbarang_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `fakturbarang` */

insert  into `fakturbarang`(`id`,`no_faktur`,`id_user`,`tanggal`,`alur`,`created_at`,`updated_at`) values 
(1,'F-1',1,'2018-10-03','Masuk','2018-10-03 04:42:35','2018-10-03 04:42:35'),
(2,'F-2',1,'2018-10-10','Keluar','2018-10-03 04:43:57','2018-10-03 04:43:57'),
(3,'F-3',2,'2019-01-02','Masuk','2019-01-05 17:40:09','2019-01-05 17:40:09'),
(4,'F-4',2,'2019-01-02','Masuk','2019-01-05 17:41:18','2019-01-05 17:41:18'),
(5,'F-5',2,'2019-01-01','Keluar','2019-01-05 17:41:39','2019-01-05 17:41:39');

/*Table structure for table `kategori` */

DROP TABLE IF EXISTS `kategori`;

CREATE TABLE `kategori` (
  `id_kategori` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(10) unsigned NOT NULL,
  `nama_kategori` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_kategori`),
  KEY `kategori_id_user_foreign` (`id_user`),
  CONSTRAINT `kategori_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `kategori` */

insert  into `kategori`(`id_kategori`,`id_user`,`nama_kategori`,`created_at`,`updated_at`) values 
(1,1,'ATK','2018-10-03 11:35:14','2018-10-03 11:35:14');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2017_12_17_142010_create_kategori_table',1),
(4,'2017_12_17_142608_create_barang_table',1),
(5,'2017_12_25_065151_FakturBarang',1),
(6,'2017_12_25_090603_alurbarang',1);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`remember_token`,`created_at`,`updated_at`) values 
(1,'admin','admin@gmail.com','$2y$10$/0COK27aX68ba8PEvq2d2OUmXuKWv9OyFG3B1oqowu/3unN1wd53m',NULL,'2018-10-03 04:31:25','2018-10-03 04:31:25'),
(2,'Dwi Yudi Rayi Anugrah','dwiyudirayia789@gmail.com','$2y$10$YhXZdb4xolxRdiibaQgrAeGIcIBo7Ghq8cKxTdUGfQuLp736ypw8a',NULL,'2019-01-05 17:33:17','2019-01-05 17:33:17');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
