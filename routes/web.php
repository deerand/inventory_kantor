<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['middleware' => 'prevent-back-history'],function(){
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

//Produk
Route::get('all/produk',[
	'uses'=>'Produk@lizt',
	'as'=>'produk.all'
]);
Route::post('/produk/store',[
	'uses'=>'Produk@store',
	'as'=>'produk.store'
]);
Route::get('produk','Produk@index');

Route::POST('filter/produk',[
	'uses'=>'Produk@getMultiFilterSelectData',
	'as'=>'produk.filter'
]);
Route::POST('produk/delete',[
	'uses'=>'Produk@destroy',
	'as'=>'produk.delete'
]);
Route::POST('produk/edit',[
	'uses'=> 'Produk@edit',
	'as' => 'produk.edit']);
//Costumer
Route::get('costumer','Costumer@index');
Route::get('filter/costumer',[
	'uses'=>'Costumer@getMultiFilterSelectData',
	'as'=>'costumer.filter'
]);
Route::POST('costumer/edit',[
	'uses'=> 'Costumer@edit',
	'as' => 'costumer.edit'
]);


//Faktur
Route::get('faktur','Faktur@index');
Route::POST('faktur/store',[
	'uses'=> 'Faktur@store',
	'as' => 'faktur.store']);


//Barang
Route::GET('barang','BarangController@index');
Route::POST('barang/store',[
	'uses'=> 'BarangController@store',
	'as' => 'barang.store']);
Route::POST('barang/datatable',[
	'uses'=> 'BarangController@anyColumnSearchData',
	'as' => 'barang.datatable']);
Route::POST('barang/hapus',[
	'uses'=> 'BarangController@destroy',
	'as' => 'barang.hapus']);
Route::POST('barang/edit',[
	'uses'=> 'BarangController@find',
	'as' => 'barang.edit']);
Route::POST('barang/update',[
	'uses'=> 'BarangController@update',
	'as' => 'barang.update']);

//Alur Barang
Route::GET('alurbarang','AlurBarangController@index');
Route::GET('typeahead/barang',[
	'uses'=> 'AlurBarangController@autocomplete',
	'as' => 'typeahead.barang']);
Route::POST('alurbarang/store',[
	'uses'=> 'AlurBarangController@store',
	'as' => 'alur.store']);

//Excel
Route::GET('export/barang/{ruangan}','KartuController@exportbarang');
});